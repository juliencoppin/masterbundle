<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 19-05-16
 */

namespace JulienCoppin\MasterBundle\Interfaces;

interface IGeneric
{
    public function __construct();

    public function validateNames();

    public function setRequiredNames();
}