<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:31
 */

namespace JulienCoppin\MasterBundle\Service;


use Doctrine\ORM\EntityManager;
use JulienCoppin\MasterBundle\Entity\CronTask;
use JulienCoppin\MasterBundle\Entity\CronTaskHistory;
use JulienCoppin\MasterBundle\Interfaces\ICronTask;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Dispatcher
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Dispatcher constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function dispatchCronTask(CronTask $cronTask, \DateTime $runTime,InputInterface $input, OutputInterface $output)
    {
        if (!$this->isRunnable($cronTask, $runTime)) {
            return;
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        if (!$this->container->has($cronTask->getCronTaskServiceName())) {
            $this->generateHistory($em, $cronTask, $runTime, false, sprintf("Unknow service name : %s", $cronTask->getCronTaskServiceName()));
            return;
        }

        try {
            $service = $this->container->get($cronTask->getCronTaskServiceName());
            if (!$service instanceof ICronTask) {
                $this->generateHistory($em, $cronTask, $runTime, false, sprintf("%s is not an instance of %s", get_class($service), ICronTask::class));
                return;
            }
            $output->writeln(sprintf("Cron task started : %s", $cronTask->getCronTaskName()));
            $start = microtime();
            $service->run($cronTask, $runTime);
            $end = microtime();
            $this->generateHistory($em, $cronTask, $runTime);
            $output->writeln(sprintf("Cron task ended : %s <info>(Execution time : %sms)</info>", $cronTask->getCronTaskName(), number_format($end - $start, 2, ',', '.')));
        } catch (\Exception $e) {
            $this->generateHistory($em, $cronTask, $runTime, false, $e->getMessage());
            $output->writeln(sprintf("An error occured. Please check the cron's history before doing anything ..."));
        }
    }

    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @return bool
     */
    private function isRunnable(CronTask $cronTask, \DateTime $runTime)
    {
        if (strcmp($cronTask->getCronTaskTime()->format('H:i'), $runTime->format('H:i')) == 0) {
            if ($cronTask->getCronTaskDayOfMonth() !== null && $cronTask->getCronTaskDayOfWeek() !== null) {
                return false;
            }
            else if ($cronTask->getCronTaskDayOfMonth() !== null) {
                return ($cronTask->getCronTaskDayOfMonth() == (int)$runTime->format('d'));
            } else if ($cronTask->getCronTaskDayOfWeek() !== null) {
                return ($cronTask->getCronTaskDayOfWeek() == (int)$runTime->format('N'));
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param EntityManager $em
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @param bool $result
     * @param string|null $message
     * @return void
     */
    private function generateHistory(EntityManager $em, CronTask $cronTask, \DateTime $runTime, $result = true, $message = null)
    {
        $history = new CronTaskHistory();
        $history->setCronTask($cronTask);
        $history->setCronTaskHistoryStartRunTime($runTime);
        $history->setCronTaskHistoryResult($result);
        $history->setCronTaskHistoryExceptionMessage($message);
        $em->persist($history);
        $em->flush();
    }
}