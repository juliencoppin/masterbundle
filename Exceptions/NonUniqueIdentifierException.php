<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 07-11-16
 * Time: 13:48
 */

namespace JulienCoppin\MasterBundle\Exceptions;


class NonUniqueIdentifierException extends \Exception
{
    /**
     * UniqueIdentifierException constructor.
     * @param string $object
     */
    public function __construct($object)
    {
        parent::__construct(sprintf("Incorrect identifier for %s supplied", is_object($object) ? get_class($object) : $object));
    }
}