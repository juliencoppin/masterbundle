<?php
/**
 * Created by PhpStorm.
 * User: Hard
 * Date: 01-05-17
 * Time: 16:26
 */

namespace JulienCoppin\MasterBundle\Helpers;


class IPHelper
{
    /**
     * @return string
     */
    public static function getUserIP()
    {
        return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }
}