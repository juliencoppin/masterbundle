<?php

namespace JulienCoppin\MasterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class JulienCoppinMasterExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!isset($config["maintenance"], $config["redirect_login"], $config["menu_builder"])) {
            throw new InvalidConfigurationException("The configuration for julien_coppin_master is invalid");
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definitionMaintenance = $container->getDefinition("juliencoppin_master.config_maintenance");
        $definitionMaintenance->addMethodCall('setConfig', array($config["maintenance"]));

        $definitionLogin = $container->getDefinition("juliencoppin_master.config_redirect_login");
        $definitionLogin->addMethodCall('setConfig', array($config["redirect_login"]));

        $definitionMenu = $container->getDefinition("juliencoppin_master.config_menu_builder");
        $definitionMenu->addMethodCall('setConfig', array($config["menu_builder"]));
    }
}
