<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:19
 */

namespace JulienCoppin\MasterBundle\Repository;


use Doctrine\ORM\EntityRepository;
use JulienCoppin\MasterBundle\Entity\CronTask;

class CronTaskRepository extends EntityRepository
{
    /**
     * @return CronTask[]
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('ct')
            ->where('ct.cronTaskActive IS TRUE');

        return $qb->getQuery()->getResult();
    }
}