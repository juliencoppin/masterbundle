<?php

namespace JulienCoppin\MasterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('julien_coppin_master');

        $rootNode
            ->children()
                // Redirect
                ->arrayNode('redirect_login')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->arrayNode("routes")
                            ->prototype("scalar")
                            ->end()
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode("redirect_to")
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                // Maintenance
                ->arrayNode('maintenance')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->arrayNode("routes")
                            ->prototype("scalar")
                            ->end()
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->arrayNode("roles")
                            ->prototype("scalar")
                            ->end()
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('menu_builder')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->scalarNode("root_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("li_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("ul_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("active_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("parent_active_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("sub_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("ul_children_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("role_namespace")
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
