<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 31-10-16
 * Time: 14:41
 */

namespace JulienCoppin\MasterBundle\Interfaces;


interface IDeletable
{
    /**
     * @return bool
     */
    public function isDeletable();
}