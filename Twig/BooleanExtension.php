<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-01-17
 * Time: 13:41
 */

namespace JulienCoppin\MasterBundle\Twig;

class BooleanExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('boolean', array($this, 'booleanFilter')),
            new \Twig_SimpleFilter('checked', array($this, 'checkedFilter')),
        );
    }

    /**
     * @param $value
     * @return bool
     */
    public function booleanFilter($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @param $value
     * @return null|string
     */
    public function checkedFilter($value)
    {
        return $this->booleanFilter($value) === true ? "checked" : null;
    }
}