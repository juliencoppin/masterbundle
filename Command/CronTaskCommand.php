<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:17
 */

namespace JulienCoppin\MasterBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronTaskCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName("juliencoppin:cron:run")
            ->setDescription("Run the cron tasks");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $runTime = new \DateTime();

        $cronTasks = $em->getRepository('JulienCoppinMasterBundle:CronTask')->findAllActive();
        $dispatcher = $this->getContainer()->get('julien_coppin_master.dispatch');

        $output->writeln("Running cron tasks ...");

        foreach ($cronTasks as $cronTask) {
            $dispatcher->dispatchCronTask($cronTask, $runTime, $input, $output);
        }
    }
}