<?php

namespace JulienCoppin\MasterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronTaskHistory
 *
 * @ORM\Table(name="CronTasksHistory")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class CronTaskHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskHistoryID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $cronTaskHistoryID;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskHistoryStartRunTime", type="datetime", nullable=false)
     */
    private $cronTaskHistoryStartRunTime;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskHistoryEndRunTime", type="datetime", nullable=false)
     */
    private $cronTaskHistoryEndRunTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CronTaskHistoryResult", type="boolean", nullable=false)
     */
    private $cronTaskHistoryResult;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskHistoryExceptionMessage", type="text", nullable=true)
     */
    private $cronTaskHistoryExceptionMessage;

    /**
     * @ORM\ManyToOne(targetEntity="JulienCoppin\MasterBundle\Entity\CronTask")
     * @ORM\JoinColumn(name="CronTaskID", referencedColumnName="CronTaskID", nullable=false)
     */
    private $cronTask;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function initDates()
    {
        if ($this->cronTaskHistoryStartRunTime === null) {
            $this->cronTaskHistoryStartRunTime = new \DateTime();
        }

        if ($this->cronTaskHistoryEndRunTime === null) {
            $this->cronTaskHistoryEndRunTime = new \DateTime();
        }
    }

    /**
     * Get cronTaskHistoryID
     *
     * @return integer
     */
    public function getCronTaskHistoryID()
    {
        return $this->cronTaskHistoryID;
    }

    /**
     * Set cronTaskHistoryStartRunTime
     *
     * @param \DateTime $cronTaskHistoryStartRunTime
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryStartRunTime($cronTaskHistoryStartRunTime)
    {
        $this->cronTaskHistoryStartRunTime = $cronTaskHistoryStartRunTime;

        return $this;
    }

    /**
     * Get cronTaskHistoryStartRunTime
     *
     * @return \DateTime
     */
    public function getCronTaskHistoryStartRunTime()
    {
        return $this->cronTaskHistoryStartRunTime;
    }

    /**
     * Set cronTaskHistoryEndRunTime
     *
     * @param \DateTime $cronTaskHistoryEndRunTime
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryEndRunTime($cronTaskHistoryEndRunTime)
    {
        $this->cronTaskHistoryEndRunTime = $cronTaskHistoryEndRunTime;

        return $this;
    }

    /**
     * Get cronTaskHistoryEndRunTime
     *
     * @return \DateTime
     */
    public function getCronTaskHistoryEndRunTime()
    {
        return $this->cronTaskHistoryEndRunTime;
    }

    /**
     * Set cronTaskHistoryResult
     *
     * @param boolean $cronTaskHistoryResult
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryResult($cronTaskHistoryResult)
    {
        $this->cronTaskHistoryResult = $cronTaskHistoryResult;

        return $this;
    }

    /**
     * Get cronTaskHistoryResult
     *
     * @return boolean
     */
    public function getCronTaskHistoryResult()
    {
        return $this->cronTaskHistoryResult;
    }

    /**
     * Set cronTaskHistoryExceptionMessage
     *
     * @param string $cronTaskHistoryExceptionMessage
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryExceptionMessage($cronTaskHistoryExceptionMessage)
    {
        $this->cronTaskHistoryExceptionMessage = $cronTaskHistoryExceptionMessage;

        return $this;
    }

    /**
     * Get cronTaskHistoryExceptionMessage
     *
     * @return string
     */
    public function getCronTaskHistoryExceptionMessage()
    {
        return $this->cronTaskHistoryExceptionMessage;
    }

    /**
     * Set cronTask
     *
     * @param \JulienCoppin\MasterBundle\Entity\CronTask $cronTask
     *
     * @return CronTaskHistory
     */
    public function setCronTask(\JulienCoppin\MasterBundle\Entity\CronTask $cronTask)
    {
        $this->cronTask = $cronTask;

        return $this;
    }

    /**
     * Get cronTask
     *
     * @return \JulienCoppin\MasterBundle\Entity\CronTask
     */
    public function getCronTask()
    {
        return $this->cronTask;
    }
}
