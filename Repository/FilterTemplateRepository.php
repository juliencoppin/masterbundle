<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04-11-16
 * Time: 13:55
 */

namespace JulienCoppin\MasterBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class FilterTemplateRepository extends EntityRepository
{
    /**
     * @var int
     */
    private $indexParam = 0;

    const PARAM_NAME = "param";

    /**
     * @param QueryBuilder $qb
     * @param $param
     * @param $field
     */
    protected function addTextParameter(QueryBuilder $qb, $param, $field)
    {
        if ($param === null) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;
        $request = sprintf("%s LIKE :%s", $field, $paramAlias);

        $qb->andWhere($request)
            ->setParameter($paramAlias, "%" . $param . "%");

        $this->indexParam++;
    }

    /**
     * @param QueryBuilder $qb
     * @param ArrayCollection $param
     * @param $field
     */
    protected function addEntityParameter(QueryBuilder $qb, ArrayCollection $param, $field)
    {
        if (count($param) == 0) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;

        $qb->andWhere(sprintf("%s IN (:%s)", $field, $paramAlias))
            ->setParameter($paramAlias, $param);

        $this->indexParam++;
    }

    /**
     * @param QueryBuilder $qb
     * @param $startDate
     * @param $endDate
     * @param $field
     */
    protected function addBetweenDateParameter(QueryBuilder $qb, $startDate, $endDate, $field)
    {
        if ($startDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere(sprintf("%s >= :%s", $field, $paramAlias))
                ->setParameter($paramAlias, $startDate);
            $this->indexParam++;
        }

        if ($endDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere(sprintf("%s <= :%s", $field, $paramAlias))
                ->setParameter($paramAlias, $endDate);
            $this->indexParam++;
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param $startDate
     * @param $endDate
     * @param array $fields
     */
    protected function addBetweenDateWithMultipleFieldsParameter(QueryBuilder $qb, $startDate, $endDate, array $fields)
    {
        if ($startDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere($this->generateQueryWithMultipleValues($paramAlias, $fields, ">="))
                ->setParameter($paramAlias, $startDate);
            $this->indexParam++;
        }

        if ($endDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere($this->generateQueryWithMultipleValues($paramAlias, $fields, "<="))
                ->setParameter($paramAlias, $endDate);
            $this->indexParam++;
        }
    }

    /**
     * @param string $paramAlias
     * @param array $fields
     * @param string $operator
     * @return string
     */
    private function generateQueryWithMultipleValues($paramAlias, array $fields, $operator)
    {
        $query = "";
        for ($i = 0; $i < count($fields); $i++) {
            if ($i < count($fields) - 1) {
                $query .= sprintf("%s %s :%s OR ", $fields[$i], $operator, $paramAlias);
            } else {
                $query .= sprintf("%s %s :%s", $fields[$i], $operator, $paramAlias);
            }
        }
        return $query;
    }

    /**
     * @param QueryBuilder $qb
     * @param $param
     * @param $field
     */
    protected function addRadioParameter(QueryBuilder $qb, $param, $field)
    {
        $value = $this->getRadioParameterAsBoolean($param);

        if ($value === null) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;

        $qb->andWhere(sprintf("%s = :%s", $field, $paramAlias))
            ->setParameter($paramAlias, $value);

        $this->indexParam++;
    }

    /**
     * @param $param
     * @return bool|null
     */
    private function getRadioParameterAsBoolean($param)
    {
        return strcmp($param, 'active') == 0 ? true : (strcmp($param, 'inactive') == 0 ? false : null);
    }
}