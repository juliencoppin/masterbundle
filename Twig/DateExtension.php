<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-01-17
 * Time: 12:01
 */

namespace JulienCoppin\MasterBundle\Twig;


class DateExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('shortDate', array($this, 'shortDateFilter')),
            new \Twig_SimpleFilter('longDate', array($this, 'longDateFilter')),
        );
    }

    /**
     * @param $value
     * @return mixed
     */
    public function shortDateFilter($value)
    {
        $value = $this->checkIfDateTime($value);

        return $value === null ? $value : $value->format('d-m-Y');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function longDateFilter($value)
    {
        $value = $this->checkIfDateTime($value);

        return $value === null ? $value : $value->format('d-m-Y H:i');
    }

    /**
     * @param $value
     * @return \DateTime
     */
    private function checkIfDateTime($value)
    {
        if (!($value instanceof \DateTime)) {
            return null;
        }

        return $value;
    }
}