<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:07
 */

namespace JulienCoppin\MasterBundle\Interfaces;


use JulienCoppin\MasterBundle\Entity\CronTask;

interface ICronTask
{
    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @return void
     */
    public function run(CronTask $cronTask, \DateTime $runTime);
}