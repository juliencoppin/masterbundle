<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-01-17
 * Time: 11:24
 */

namespace JulienCoppin\MasterBundle\Service\Configuration;

class MaintenanceParameter
{
    /**
     * @var array
     */
    private $routes;

    /**
     * @var array
     */
    private $roles;

    public function setConfig($config)
    {
        $this->routes = $config["routes"];
        $this->roles = $config["roles"];
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }
}