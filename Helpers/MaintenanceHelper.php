<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-01-17
 * Time: 13:51
 */

namespace JulienCoppin\MasterBundle\Helpers;

use Doctrine\Common\Persistence\ObjectManager;
use JulienCoppin\MasterBundle\Entity\GlobalParameter;

class MaintenanceHelper
{
    /**
     * @param ObjectManager $manager
     * @return GlobalParameter
     */
    public static function getMaintenance(ObjectManager $manager)
    {
        $item = $manager->getRepository('JulienCoppinMasterBundle:GlobalParameter')->findOneBy(array('globalParameterLabel' => 'maintenance'));
        if ($item === null) {
            $item = new GlobalParameter();
            $item->setGlobalParameterLabel('maintenance');
            $item->setGlobalParameterValue('false');
            $manager->persist($item);
            $manager->flush();
        }
        return $item;
    }
}