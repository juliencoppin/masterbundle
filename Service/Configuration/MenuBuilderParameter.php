<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 03-11-16
 * Time: 16:15
 */

namespace JulienCoppin\MasterBundle\Service\Configuration;


class MenuBuilderParameter
{
    /**
     * @var string
     */
    private $root_class;

    /**
     * @var string
     */
    private $li_class;

    /**
     * @var string
     */
    private $ul_class;

    /**
     * @var string
     */
    private $active_class;

    /**
     * @var string
     */
    private $parent_active_class;

    /**
     * @var string
     */
    private $sub_class;

    /**
     * @var string
     */
    private $ul_children_class;

    /**
     * @var string
     */
    private $role_namespace;

    public function setConfig($config)
    {
        $this->root_class = $config["root_class"];
        $this->li_class = $config["li_class"];
        $this->ul_class = $config["ul_class"];
        $this->active_class = $config["active_class"];
        $this->parent_active_class = $config["parent_active_class"];
        $this->sub_class = $config["sub_class"];
        $this->ul_children_class = $config["ul_children_class"];
        $this->role_namespace = $config["role_namespace"];
    }

    /**
     * @return string
     */
    public function getRootClass()
    {
        return $this->root_class;
    }

    /**
     * @return string
     */
    public function getLiClass()
    {
        return $this->li_class;
    }

    /**
     * @return string
     */
    public function getUlClass()
    {
        return $this->ul_class;
    }

    /**
     * @return string
     */
    public function getActiveClass()
    {
        return $this->active_class;
    }

    /**
     * @return string
     */
    public function getParentActiveClass()
    {
        return $this->parent_active_class;
    }

    /**
     * @return string
     */
    public function getSubClass()
    {
        return $this->sub_class;
    }

    /**
     * @return string
     */
    public function getUlChildrenClass()
    {
        return $this->ul_children_class;
    }

    /**
     * @return string
     */
    public function getRoleNamespace()
    {
        return $this->role_namespace;
    }
}