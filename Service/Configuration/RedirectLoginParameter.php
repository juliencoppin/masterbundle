<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-01-17
 * Time: 15:05
 */

namespace JulienCoppin\MasterBundle\Service\Configuration;


class RedirectLoginParameter
{
    /**
     * @var array
     */
    private $routes;

    /**
     * @var string
     */
    private $redirectTo;

    public function setConfig(array $config)
    {
        $this->routes = $config['routes'];
        $this->redirectTo = $config['redirect_to'];
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @return string
     */
    public function getRedirectTo()
    {
        return $this->redirectTo;
    }
}