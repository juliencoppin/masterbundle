<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04-11-16
 * Time: 12:41
 */

namespace JulienCoppin\MasterBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class FilterFormType extends CustomSaveFormType
{
    public function setRequiredNames()
    {
        $this->ignoreDataClass = true;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('empty', ButtonType::class, array(
          'label' => 'empty',
          'attr' => array('class' => 'btn btn-block btn-primary clear-form')
        ));
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('save_label', 'search');
    }
}