<?php

namespace JulienCoppin\MasterBundle\Interfaces;

interface IGenericRepository
{
    public function findAllWithRequiredData();

    public function findOneWithRequiredData($itemID);

    public function findForDelete($itemID);
}