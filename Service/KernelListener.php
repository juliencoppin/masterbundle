<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 25-05-16
 * Time: 12:46
 */

namespace JulienCoppin\MasterBundle\Service;

use JulienCoppin\MasterBundle\Helpers\MaintenanceHelper;
use JulienCoppin\MasterBundle\Service\Configuration\MaintenanceParameter;
use JulienCoppin\MasterBundle\Service\Configuration\RedirectLoginParameter;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class KernelListener
{
    /**
     * @var AuthorizationChecker
     */
    private $security;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * @var MaintenanceParameter
     */
    private $maintenanceParams;

    /**
     * @var RedirectLoginParameter
     */
    private $redirectLoginParams;

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $templating;

    /**
     * KernelListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->security = $container->get('security.authorization_checker');
        $this->router = $container->get('router');
        $this->tokenStorage = $container->get('security.token_storage');
        $this->maintenanceParams = $container->get('juliencoppin_master.config_maintenance');
        $this->redirectLoginParams = $container->get('juliencoppin_master.config_redirect_login');
        $this->em = $container->get('doctrine')->getManager();
        $this->templating = $container->get('templating');
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if (in_array($route, $this->redirectLoginParams->getRoutes()) && $this->security->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $event->setResponse(new RedirectResponse($this->router->generate($this->redirectLoginParams->getRedirectTo())));
            return;
        }

        $maintenance = MaintenanceHelper::getMaintenance($this->em);
        if (filter_var($maintenance->getGlobalParameterValue(), FILTER_VALIDATE_BOOLEAN) === true){
            if (!in_array($route, $this->maintenanceParams->getRoutes())){
                if ($this->tokenStorage->getToken() === null || !$this->security->isGranted($this->maintenanceParams->getRoles())) {
                    $event->setResponse(new Response($this->templating->render("JulienCoppinMasterBundle:Maintenance:maintenance.html.twig")));
                    return;
                }
            }
        }
    }
}