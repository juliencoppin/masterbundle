<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 19-05-16
 */

namespace JulienCoppin\MasterBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class CustomSaveFormType extends CustomFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('save', SubmitType::class, array(
          'label' => $options['save_label'] === null ? 'save' : $options['save_label'],
          'attr' => array('class' => 'btn btn-block btn-success')
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}